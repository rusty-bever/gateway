//! The db module contains all Diesel-related logic. This is to prevent the various Diesel imports
//! from poluting other modules' namespaces.

pub mod tokens;
pub mod users;

pub use tokens::{NewRefreshToken, RefreshToken};
pub use users::{NewUser, User};
