# rb-gw

The gateway service serves several functions:

* Make the microservice architecture appear as a single API
* Route requests to their respective microservice
* Authenticate requests

This service is the only one that the end user will use & is also the only one
that allows connections from non-microservice sources.
