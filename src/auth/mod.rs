use serde::Deserialize;

pub mod jwt;
pub mod pass;

#[derive(Deserialize)]
pub struct Credentials
{
    pub username: String,
    pub password: String,
}
