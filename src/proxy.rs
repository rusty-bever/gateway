use rocket::{
    http::Method,
    response::Redirect,
    route::{Handler, Outcome},
    Data, Request, Route,
};

#[derive(Clone)]
pub struct ProxyServer
{
    root: String,
    rank: isize,
}

impl ProxyServer
{
    const DEFAULT_RANK: isize = 0;

    pub fn new(root: String, rank: isize) -> Self
    {
        ProxyServer { root, rank }
    }

    pub fn from(root: String) -> Self
    {
        Self::new(root, Self::DEFAULT_RANK)
    }
}

impl Into<Vec<Route>> for ProxyServer
{
    fn into(self) -> Vec<Route>
    {
        let mut routes: Vec<Route> = vec![];

        static METHODS: [Method; 5] = [
            Method::Get,
            Method::Post,
            Method::Patch,
            Method::Delete,
            Method::Put,
        ];

        for method in METHODS {
            let mut route = Route::ranked(self.rank, method, "/<path..>", self.clone());
            route.name =
                Some(format!("ProxyServer: {} {}", method.as_str(), self.root.clone()).into());

            routes.push(route);
        }

        routes
    }
}

#[rocket::async_trait]
impl Handler for ProxyServer
{
    async fn handle<'r>(&self, req: &'r Request<'_>, data: Data<'r>) -> Outcome<'r>
    {
        todo!()
    }
}
