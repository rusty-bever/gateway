use rb::{errors::RbResult, guards::User};
use rb_gw::auth::{
    jwt::{generate_jwt_token, JWTResponse},
    pass::verify_user,
    Credentials,
};
use rocket::{serde::json::Json, State};
use serde::Deserialize;

use crate::{RbConfig, RbDbConn};

#[post("/login")]
pub async fn already_logged_in(_user: User) -> String
{
    String::from("You're already logged in!")
}

#[post("/login", data = "<credentials>", rank = 2)]
pub async fn login(
    conn: RbDbConn,
    conf: &State<RbConfig>,
    credentials: Json<Credentials>,
) -> RbResult<Json<JWTResponse>>
{
    let credentials = credentials.into_inner();
    let jwt = conf.jwt.clone();

    // Get the user, if credentials are valid
    let user = conn
        .run(move |c| verify_user(c, &credentials.username, &credentials.password))
        .await?;

    Ok(Json(
        conn.run(move |c| generate_jwt_token(c, &jwt, &user))
            .await?,
    ))
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RefreshTokenRequest
{
    pub refresh_token: String,
}

#[post("/refresh", data = "<refresh_token_request>")]
pub async fn refresh_token(
    conn: RbDbConn,
    conf: &State<RbConfig>,
    refresh_token_request: Json<RefreshTokenRequest>,
) -> RbResult<Json<JWTResponse>>
{
    let refresh_token = refresh_token_request.into_inner().refresh_token;
    let jwt = conf.jwt.clone();

    Ok(Json(
        conn.run(move |c| rb_gw::auth::jwt::refresh_token(c, &jwt, &refresh_token))
            .await?,
    ))
}
